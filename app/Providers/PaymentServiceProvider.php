<?php

namespace App\Providers;

use App\Services\Payments\Apple\ApplePaymentHandler;
use App\Services\Payments\Contracts\PaymentHandlerInterface;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentHandlerInterface::class, function () {
            return new ApplePaymentHandler();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

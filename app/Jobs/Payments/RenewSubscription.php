<?php

namespace App\Jobs\Payments;

use App\Jobs\Payments\Contracts\ProcessableDataInterface;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;
use App\Services\Payments\Repositories\SubscriptionRepository;
use App\Services\Payments\Repositories\TransactionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RenewSubscription implements ShouldQueue, ProcessableDataInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $processableData;

    public function setData(ProcessablePaymentDataInterface $data)
    {
        $this->processableData = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptionRepo = new SubscriptionRepository();
        $transactionRepository = new TransactionRepository();

        $subscription = $subscriptionRepo->findByExternalKey($this->processableData['subscription_identification_key']);
        $subscriptionRepo->renew($this->processableData, $subscription);
        $transactionRepository->store($this->processableData, $subscription);

        //maybe reset download counter or smth? (counts of product use time?)
    }

}

<?php

namespace App\Jobs\Payments;

use App\Jobs\Payments\Contracts\ProcessableDataInterface;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;
use App\Services\Payments\Repositories\SubscriptionRepository;
use App\Services\Payments\Repositories\TransactionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InitSubscription implements ShouldQueue, ProcessableDataInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $processedData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptionRepository = new SubscriptionRepository();
        $transactionRepository = new TransactionRepository();

        $subscription =  $subscriptionRepository->create($this->processedData);

        $transactionRepository->store($this->processedData, $subscription);

        $subscriptionRepository->start($subscription);

        //maybe send letter that u can now use everything?
    }

    public function setData(ProcessablePaymentDataInterface $data)
    {
        $this->processedData = $data;
    }
}

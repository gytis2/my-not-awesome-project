<?php

namespace App\Jobs\Payments;

use App\Jobs\Payments\Contracts\ProcessableDataInterface;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;
use App\Services\Payments\Repositories\SubscriptionRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CancelSubscription implements ShouldQueue, ProcessableDataInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $processedData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
          $subscriptionRepo = new SubscriptionRepository();
          $subscription = $subscriptionRepo->findByExternalKey($this->processedData['subscription_identification_key']);
          $subscriptionRepo->cancel($subscription);

          //maybe send letter that u canceled?
    }

    public function setData(ProcessablePaymentDataInterface $data)
    {
        $this->processedData = $data;
    }
}

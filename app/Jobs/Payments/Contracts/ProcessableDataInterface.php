<?php


namespace App\Jobs\Payments\Contracts;


use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;

interface ProcessableDataInterface
{
    public function setData(ProcessablePaymentDataInterface $data);
}

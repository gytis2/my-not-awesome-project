<?php


namespace App\Models;


class Transaction
{
    protected $fillable = [
        'user_id',
        'provider_id',
        'success'
    ];
}

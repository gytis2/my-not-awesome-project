<?php


namespace App\Services\Payments\Apple\Exceptions;


use Exception;

class ApplePayExceptions extends Exception
{
    public static function invalidSharedKey(): ApplePayExceptions
    {
        return new static('Your shared key is invalid');
    }

}

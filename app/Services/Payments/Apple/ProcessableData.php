<?php


namespace App\Services\Payments\Apple;


use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;

class ProcessableData implements ProcessablePaymentDataInterface
{
    protected $unprocessedData;

    public function __construct($unprocessedData)
    {
        $this->unprocessedData = $unprocessedData;
    }

    public function data(): array
    {
        return [
            'user_id' => $this->unprocessedData['user_id'],
            'provider_id' => $this->unprocessedData['provider_id'],
            'notification_type' => $this->unprocessedData['notification_type'],
            'subscription_identification_key' => $this->unprocessedData['auto_renew_adam_id'],
            'renewed_product_id' => $this->unprocessedData['auto_renew_product_id'],
            'product_id' => $this->unprocessedData['product_id'],
            'latest_receipt' => $this->unprocessedData['unified_receipt']['latest_receipt'],
            'renew_status' => $this->unprocessedData['auto_renew_status'],
            'renew_status_change_date' => $this->unprocessedData['auto_renew_status_change_date']
        ];
    }
}

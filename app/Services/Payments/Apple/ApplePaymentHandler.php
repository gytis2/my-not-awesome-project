<?php


namespace App\Services\Payments\Apple;


use App\Services\Payments\Contracts\PaymentHandlerInterface;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;
use App\Services\Payments\Repositories\NotificationRepository;
use App\Services\Payments\Repositories\TransactionRepository;

class ApplePaymentHandler implements PaymentHandlerInterface
{

    public function handle(ProcessablePaymentDataInterface $data)
    {
        $payload = $data->data();
        $processor = config("subscriptions.processors.{$payload['notification_type']}");
        $processingJob = new $processor();
        $processingJob->setData($payload);
        dispatch($processingJob);
    }

    public function storeNotification(ProcessablePaymentDataInterface $data)
    {
        (new NotificationRepository())->store($data);
    }
}

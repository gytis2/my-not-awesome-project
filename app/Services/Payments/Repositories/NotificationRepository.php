<?php


namespace App\Services\Payments\Repositories;


use App\Models\PaymentNotification;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;

class NotificationRepository
{

    public function store(ProcessablePaymentDataInterface $processablePaymentData)
    {
        $data = $processablePaymentData->data();

        return PaymentNotification::create([
            'user_id' => $data['user_id'],
            'provider_id' => $data['provider_id'],
            'action' => $data['notification_type'],
            'payload' => json_encode($data)
        ]);
    }

}

<?php


namespace App\Services\Payments\Repositories;


use App\Models\Subscription;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;

class SubscriptionRepository
{
    public function cancel(Subscription $subscription)
    {
        $subscription->update([
            'active' => 0,
            'canceled_at' => now()->toDateTimeString()
        ]);
    }

    public function renew(ProcessablePaymentDataInterface $data, Subscription $subscription)
    {
        $processedData = $data->data();

        $subscription->update([
            'active' => 1,
            'valid_to' => $processedData['duration'] ?? now()->addMonthNoOverflow()->toDateTimeString(),
            'renew_status' => $processedData['renew_status'],
            'last_renewed_at' => now()->toDateTimeString()
        ]);
    }

    public function start(Subscription $subscription)
    {
       return $subscription->update([
            'active' => 1,
            'valid_to' => $processedData['duration'] ?? now()->addMonthNoOverflow()->toDateTimeString()
        ]);
    }

    public function create(ProcessablePaymentDataInterface $data)
    {
        return Subscription::create($data->data());
    }

    public function markAsRenewFailed(Subscription $subscription)
    {
        $subscription->update([
            'active' => 0,
            'valid_to' => null
        ]);
    }


    public function findByExternalKey($externalKey)
    {
       return Subscription::where([
           'external_key' => $externalKey
       ])->first();
    }
}

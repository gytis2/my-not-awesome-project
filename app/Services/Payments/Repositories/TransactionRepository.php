<?php


namespace App\Services\Payments\Repositories;


use App\Models\Subscription;
use App\Models\Transaction;
use App\Services\Payments\Contracts\ProcessablePaymentDataInterface;

class TransactionRepository
{
    public function store(ProcessablePaymentDataInterface $processableData, Subscription $subscription)
    {
        $data = $processableData->data();

        return Transaction::create([
            'user_id' => $data['user_id'],
            'provider_id' => $data['provider_id'],
            'product_id' => $data['product_id'],
            'subscription_id' => $subscription->id,
            //dummy
            'before_tax' => 0,
            'after_tax' => 0,
            'vat_percent' => 21,
            'status' => $data['status']
        ]);
    }
}

<?php


namespace App\Services\Payments\Contracts;


Interface PaymentHandlerInterface
{
    public function handle(ProcessablePaymentDataInterface $data);

    public function storeNotification(ProcessablePaymentDataInterface $data);
}

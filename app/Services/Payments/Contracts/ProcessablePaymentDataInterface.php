<?php


namespace App\Services\Payments\Contracts;


Interface ProcessablePaymentDataInterface
{

    public function __construct($unprocessedData);

    public function data(): array;
}

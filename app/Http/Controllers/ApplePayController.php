<?php


namespace App\Http\Controllers;


use App\Http\Requests\ApplePayWebhookRequest;
use App\Services\Payments\Apple\ProcessableData;
use App\Services\Payments\Contracts\PaymentHandlerInterface;
use Illuminate\Support\Facades\Auth;

class ApplePayController extends Controller
{
    protected $paymentHandler;

    public function __construct(PaymentHandlerInterface $paymentHandler)
    {
        $this->paymentHandler = $paymentHandler;
    }

    public function handle(ApplePayWebhookRequest $request, Auth $auth)
    {
        $request->request->add([
            'user_id' => $auth->user()->id,
            'provider_id' => 1
        ]);

        $processablePaymentData = new ProcessableData($request->validated());

        $this->paymentHandler->storeNotification($processablePaymentData);

        return $this->paymentHandler->handle($processablePaymentData);
    }
}

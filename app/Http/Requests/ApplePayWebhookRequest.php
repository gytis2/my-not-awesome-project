<?php


namespace App\Http\Requests;


use App\Services\Payments\Apple\Exceptions\ApplePayExceptions;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ApplePayWebhookRequest extends FormRequest
{

    public function authorize()
    {
        if($this->get('password') !== config('applePay.secret')){
            throw ApplePayExceptions::invalidSharedKey();
        }
        return true;
    }

    public function rules(): array
    {
        return [
            'notification_type' => ['required', 'string', Rule::in(config('applePay.allowed_webhook_types'))],
            'password' => ['required', 'string']
        ];
    }
}

<?php

return [
    'secret' => env('APPLE_PAY_SHARED_SECRET', 'KILO_MUST_BE_HEALTH'),

    'allowed_webhook_types' => [
        'INITIAL_BUY',
        'DID_RENEW',
        'DID_FAIL_TO_RENEW',
        'CANCEL'
    ],
];

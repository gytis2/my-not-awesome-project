<?php

use App\Jobs\Payments\CancelSubscription;
use App\Jobs\Payments\FailedRenewSubscription;
use App\Jobs\Payments\InitSubscription;
use App\Jobs\Payments\RenewSubscription;

return [
    'processors' => [
        'INITIAL_BUY' => InitSubscription::class,
        'DID_RENEW' => RenewSubscription::class ,
        'DID_FAIL_TO_RENEW' => FailedRenewSubscription::class,
        'CANCEL' => CancelSubscription::class
    ]
];

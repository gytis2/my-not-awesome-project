<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->index();
            $table->bigInteger('external_key')->index();
            $table->bigInteger('latest_receipt')->index();
            $table->integer('product_id')->index();
            $table->boolean('active')->default(0);
            $table->dateTime('valid_to')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->dateTime('last_renewed_at')->nullable();
            $table->boolean('renew_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
